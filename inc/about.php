<section id="about" class="on">
	<table id="table_about">
		<tbody>
			<tr class="col-letter" id="btnAbout">
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<th class="letter" data-letter="&nbsp;"><span>&nbsp;</span></th>
				<td class="letter" data-letter="A"><span>A</span></td>
				<td class="letter" data-letter="B"><span>B</span></td>
				<td class="letter" data-letter="É"><span>É</span></td>
				<td class="letter" data-letter="C"><span>C</span></td>
				<td class="letter" data-letter="É"><span>É</span></td>
				<td class="letter" data-letter="D"><span>D</span></td>
				<td class="letter" data-letter="A"><span>A</span></td>
				<td class="letter" data-letter="I"><span>I</span></td>
				<td class="letter" data-letter="R"><span>R</span></td>
				<td class="letter" data-letter="E"><span>E</span></td>
			</tr>
		</tbody>
	</table>
	<div class="polyLeft"></div>
	<div id="intro" class="intro">
		<p>Le 75 a 50 ans !</p>
		<p>À cette occasion nous avons conçu ce site recueillant les témoignages et réflexions de ceux et celles qui sont passé.es par l’<a href="http://www.leseptantecinq.be/fr/activites/50-ans-de-lesa-le-75/">ESA LE 75</a>.</p>
		<p>Nous leur avons suggéré un abécédaire de thèmes, dans l’idée de faire entendre un discours sur la finalité de l’enseignement de l’art, non pas un discours historique ou théorique mais bien une mosaïque de voix/voies hétérogènes, autonomes qui néanmoins peuvent se croiser.</p>
		<p>Cet abécédaire est une archive de l’école et de son activité pédagogique et artistique.</p>
		<p>Bonne découverte !</p><br><br>
	</div>
	<div class="polyRight"></div>
	<div class="generique">
		<p class="end">Un projet collectif et interdisciplinaire de l’ESA LE 75 avec la participation active de <a href="http://www.septembretiberghien.com/">Septembre Tiberghien</a> et de <a href="http://www.luuse.io/">Luuse</a>.</p>
		<p class="end">Merci à celles et ceux qui ont répondu à l’appel, à savoir :</p>
		<p> <?php $i = 1;
			foreach($videos->files as &$n): ?>
					<span class="contrib" data-ext="<?= $n->extension; ?>" data-href="abcd_<?= $n->word; ?>_<?= $n->numero; ?>" ><?= $n->name; ?> <?= $n->surname; ?></span><?php if (count($videos->files) !== $i){ echo ','; $i++;}; ?>
			<?php endforeach ?> </p>
	</div>
	<div class="imgSwitch">
		<a href="http://www.leseptantecinq.be/fr/"><img class="imgLogo"src="css/img/logo50_color.png"/></a>
		<a href="http://www.leseptantecinq.be/fr/"><img class="imgChange" src="css/img/logo50_sans_fond.png"/></a>
	</div>
</section>
