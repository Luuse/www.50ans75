	<section id="list" class="off" >
		<table id="table_list" class="show">
			<nav>
				<input type="button" id="btnMuted" value="muted" />

			</nav>
			<tr class="col-letter" >

<?php
$l = $list->list;
for ($i = 0; $i < count($l); $i++): ?>
					<th
					class="letter itn"
					data-letter="<?= $l[$i]->letter; ?>"><span><?= $l[$i]->letter; ?></span></th>
			<?php	endfor; ?>
				<th class="letter itn"  id="btnList" data-letter="infos"><span>?</span></th>
			</tr>
<?php $l = $list->list;
for($n = 1; $n < 4; $n++): ?>
			<tr class="col-words">
<?php for ($i = 0; $i <= count($l); $i++):
$vs = $videos->search($l[$i]->word[$n]);
if ($vs !== null): ?>
			<td class="Called word itn hidden"
					data-letter="<?= $l[$i]->letter; ?>"
				>
				<?php
					$wi = 1;
					foreach($vs as &$v): ?>
					<span
					class="int_word"
					id="<?= $v->word .'_'. $wi; ?>"	
					data-href="<?= $v->path; ?>"
					data-extension="<?= $v->files->extension; ?>"
					data-name="<?= $v->name; ?>"
					data-surname="<?= $v->surname; ?>"
					data-word="<?= $v->word; ?>"
					data-author="<?= $v->name . '  ' . $v->surname . ' ~ ' . $v->word; ?>"
					><?= $l[$i]->word[$n]; ?></span>
				<?php
					$wi++;
					endforeach; ?>
			</td>
			<?php else:  ?>
					<td class="notCalled word itn hidden"
							data-letter="<?= $l[$i]->letter; ?>"
					 >
						<span data-letter="<?= $l[$i]->letter; ?>" ><?= $l[$i]->word[$n]; ?></span>
					</td>
			<?php	endif; ?>
				<?php	endfor; ?>
			</tr>
		<?php endfor; ?>

		</table>
		<video id="lecteurV" loop class="onfront">
		</video>

		<audio autoplay muted loop id="lecteurA" src=""> /</audio>
		<table id="infomedia">
			<tr>
				<td id="info_name" ></td>
				<td id="info_surname" ></td>
				<td id="info_word" ></td>
			</tr>
		</table>

		</section>
