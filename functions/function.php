<?php

class videos {

  public function list_words()
  {
		$list_url = 'texts/liste.txt';

		if ($fh = fopen($list_url, 'r')) {
			$i = 0;
			$list = [];
			while (!feof($fh)) {
					$line = fgets($fh);
					// $line = str_replace("\n", "", htmlentities($line));
					$xl = explode(',', $line);
					$letter = $xl[0];

					if($letter !== '') {

						$this->list[$i]->letter = $letter;

						for($u = 1; $u < count($xl); $u++) {
							$this->list[$i]->word[$u] = $xl[$u];
						}
						$i = $i + 1;
					}
			}
			fclose($fh);
		}
		// echo '<pre>';
		// print_r($this->list);
		// echo '</pre>';
		return $this;
	}

  public function items($dir)
  {
		$i = 0;
		$files = [];


		if(is_dir($dir)) {

			$this->direction = $dir;

			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					 if($file != '.' && $file != '..' && preg_match('#\.(avi|mov|m4a|mp4|m4v|mp3)$#i', $file)) {

						$fl = explode('.', $file);
						$ext = end($fl);
						$sp = explode("_", $fl[0]);
						$name = explode("-", $sp[0]);
						$name = str_replace('~~', '-', $name);
						$name = str_replace('~', ' ', $name);
						$this->files[$i]->path = $dir . $file;
						$this->files[$i]->name = $name[0];
						$this->files[$i]->surname = $name[1];
						$this->files[$i]->word = $sp[1];
						if ($vv[$sp[1]] == null) {
							$vv[$sp[1]] = 1;
						} else {
							$vv[$sp[1]] = 2;
						}
						$this->files[$i]->numero= $vv[$sp[1]];
						$this->files[$i]->extension = $ext;

						$i = $i + 1;
					}
				}
				closedir($dh);
			}
		}
		return $this;
  }

	public function search($word) {
		$n = new videos;
		$videos = $n->items('videos/');
		$result = [];
		foreach($videos->files as &$f){
			if($word == $f->word){
				$result[] = $f;
			};
		};
		return isset($result[0]) ? $result : null;
	}
}

$v = new videos;
$videos = $v->items('videos/');

$l = new videos;
$list = $l->list_words();


