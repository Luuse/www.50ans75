let body         = document.getElementsByTagName('BODY')[0]
let nav          = document.getElementsByTagName('nav')[0]
let btnAbout     = document.getElementById('btnAbout')
let btnList      = document.getElementById('btnList')
let contrib      = document.getElementsByClassName('contrib')
let list         = document.getElementById('list')
let about        = document.getElementById('about')
let ths 	     = document.getElementsByClassName('itn')
let tabList      = document.getElementById('table_list')
let called       = document.querySelectorAll('.int_word')
let lecteurV     = document.getElementById('lecteurV')
let lecteurA     = document.getElementById('lecteurA')
let infomedia    = document.getElementById('infomedia')
let info_name    = document.getElementById('info_name')
let info_surname = document.getElementById('info_surname')
let info_word    = document.getElementById('info_word')
let btnMuted     = document.getElementById('btnMuted')
let contentIntro = document.getElementById('intro')
let tableAbout   = document.getElementById('table_about')

function addHash(href) {
	var file = href
	var h = 'abcd_' + file
	var res = escape(h)
	window.location.hash = res
}

function changePage(hash) {
	if (about.className == 'on') {
		about.className = 'off'
		list.className = 'on'
		loadTable('load')
		btnList.value = 'info'
		lecteurV.pause()
		lecteurA.pause()
		window.location.hash = hash
	} else {
		about.className = 'on'
		list.className = 'off'
		loadTable('unload')
		btnList.value = 'liste'
		lecteurV.pause()
		lecteurA.pause()
		window.location.hash = ''
	}
}

function muted() {
	function mutedAction(){
		if(lecteurV.muted || lecteurA.muted) {
			lecteurV.muted = false
			lecteurA.muted = false
			this.value = 'muted'
		} else {
			lecteurV.muted = true
			lecteurA.muted = true
			this.value = 'unmuted'
		}
	}
	btnMuted.addEventListener('click', mutedAction, false)
}

function syncTable(){
	for(let th of ths) {
		th.addEventListener('mouseover', function(){
			var dlsOld = document.querySelectorAll('[data-letter]')
			for(let dlOld of dlsOld) {
				dlOld.classList.remove("hovered")
			}
			var lt = th.getAttribute('data-letter')
			if (lt) {
				var dls = document.querySelectorAll('[data-letter=' + lt + ']')
				for(let dl of dls) {
					dl.classList.add("hovered")
				}
			}
		})
		th.addEventListener('mouseleave', function(){
			var dlsOld = document.querySelectorAll('[data-letter]')
			for(let dlOld of dlsOld) {
				dlOld.classList.remove("hovered")
			}
		})
	}
}

function showList(){
	if(window.innerWidth <= 800){
		var t = 8000
	}else{
		var t = 3000
	}
	var x
	body.addEventListener("mousemove", function(){
		if (x) clearTimeout(x)
		lecteurV.className = 'onback'
		lecteurA.className = 'onback'
		tabList.className = 'show'
		nav.classList.remove('hidden')
		body.classList.remove('dark')
		body.className = 'light'

		if (tabList.parentElement.className == 'on') {
			x = setTimeout(function(){
				nav.className = 'hidden'
				tabList.className = 'hidden'
				lecteurV.classList.remove('onback')
				lecteurV.className = 'onfront'
				lecteurA.classList.remove('onback')
				lecteurA.className = 'onfront'
				body.classList.remove('light')
				body.className = 'dark'
			}, t)
		}
	}, false)
}

function playMedia(ext, href) {
	if ( ext == 'mp3' || ext == 'm4a') {
		lecteurV.innerHTML = ""
		lecteurV.pause()
		lecteurV.style.visibility = "hidden"
		lecteurA.setAttribute('src', href)
		lecteurA.currentTime = 0
	} else {
		var source = document.createElement('source')
		lecteurA.pause()
		lecteurA.currentTime = 0
		lecteurV.pause()
		lecteurV.innerHTML = ""
		lecteurV.style.visibility = "visible"
		source.setAttribute('src', href)
		lecteurV.appendChild(source)
		lecteurV.load()
		lecteurV.play()
	};
}

function contriLoad() {
	var ln = function() {
		var href = this.getAttribute('data-href')
		var ext = this.getAttribute('data-ext')
		changePage(escape(href))
		loadHash()
	}
	for (var i = 0; i < contrib.length; i++) {
		contrib[i].addEventListener('click', ln, false)
	}
}

function infoFooter(elem, fixed) {
	var name = elem.getAttribute('data-name')
	info_name.innerHTML = name
	var surname = elem.getAttribute('data-surname')
	info_surname.innerHTML = surname
	var word = elem.getAttribute('data-word')
	info_word.innerHTML = word
	var colorComputed = window.getComputedStyle(elem, null).getPropertyValue("color")
	infomedia.style.color = colorComputed
	if (fixed == true) {
		var id = elem.getAttribute('id')
		infomedia.setAttribute('data-id', id)
	}
}

function loadMedia(){
	var loadAction = function() {
		var id = this.getAttribute('id')
		var href = this.getAttribute('data-href')
		var ext = this.getAttribute('data-extension')
		infoFooter(this, true)
		playMedia(ext, href)
		addHash(id)
	}
	for (var i = 0; i < called.length; i++) {
		called[i].addEventListener('click', loadAction, false)
	}
}
function hoverMedia(){
	var hoverAction = function() {
		infoFooter(this, false)
	}
	for (var i = 0; i < called.length; i++) {
		called[i].addEventListener('mouseover', hoverAction, false)
	}
	var hoverLeave = function() {
		var idName = infomedia.getAttribute('data-id')
		var id = document.getElementById(idName)

		infoFooter(id, false)
	}
	for (var i = 0; i < called.length; i++) {
		called[i].addEventListener('mouseleave', hoverLeave, false)
	}
}

function Shuffle(o) {
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x)
		return o
}

function loadTable(order) {
	var tds = document.querySelectorAll('#list td')
	var counter = 0

	if (order == 'load') {
		var t = []
		for (var u = 0, len = tds.length; u < len; u++) {
			t.push(u)
		}
		t = Shuffle(t)
		function tdShow(y) {
			tds[t[y]].classList.remove('hidden')
			tds[t[y+1]].classList.remove('hidden')
			tds[t[y+2]].classList.remove('hidden')
			tds[t[y+3]].classList.remove('hidden')
		}
		for (y = 0; y <= t.length; y = y + 4 ) {
			var rdm = Math.floor(Math.random(10) * Math.floor(30))
			setTimeout(tdShow, y * rdm, y)
		}
		setTimeout(showList, (t.length * 3) * rdm)
	}else if (order == 'unload') {
		for (var u = 0, len = tds.length; u < len; u++) {
			tds[u].classList.add('hidden')
		}
	}
}

function introTable() {
	var p = contentIntro.getElementsByTagName('p')
	for (var i = 0; i < p.length; i++) {
		var cont = p[i].innerText
		var words = cont.split(' ')
		var newTr = document.createElement('TR')
		words.forEach(function(e, ind) {
			if( ind < 27 ) {
				newTr.innerHTML += '<td><span style="margin-top: ' + (ind * 1) + 'px">' + e + '</span></td>'
			}
		})
		tableAbout.children[0].innerHTML += newTr.outerHTML
	}
}


function loadHash() {
	var href = window.location.href.match(/^[^#]+#([^?]*)\??(.*)/)
	if(href !== null) {
		href = unescape(href[1])

		var hash = href.split('_')[0]
		if ( hash == 'abcd' ){
			about.className = 'off'
			list.className = 'on'
			loadTable('load')
		}
		if(typeof href.split('_')[1] !== 'undefined' && typeof href.split('_')[2] !== 'undefined' ) {
			var id = href.split('_')[1] + '_' + href.split('_')[2]
			var elem = document.getElementById(id)
			var file = elem.getAttribute('data-href')
			var ext = elem.getAttribute('data-extension')
			infoFooter(elem, true)
			setTimeout(function() {
				playMedia(ext, file)
			}, 2000)
		}
	}
}

function randomParty() {
	var c = parseInt(Math.random() * called.length)
	var ext = called[c].getAttribute('data-extension')
	var href = called[c].getAttribute('data-href')
	infoFooter(called[c], true)
	playMedia(ext, href)
}
